# About

This is a repository for OFE's Rising Costs project. 

The data appendices, data sets, and code are available for download.

----

## The files

The file names for all resources in this repository are listed below.

1. The appendices
  - (forthcoming `appendix.md`)
  - (forthcoming Word document)

2. Data: 
  - CPI-U All Items, `all-items-202304` 
  - CPI-U Food and Beverage Items, `food-bev-202304` 
  - Consumer Expenditures, `ce-food-202112` 

3. Code: 
  - (forthcoming)
